import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service'

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
name:string;
age:number;
email:string;
address:Address;
hobbies = ['coding' , 'swimming' , 'playing bing ball'];
  constructor(private dataservice:DataService) {
  }

  ngOnInit() {
    this.name = 'hammam';
    this.age = 10;
    this.email = 'h@h.com';
    this.address = {
      street = 'mohammed bin abdulaziz',
      city = 'jeddah',
      state = 'makkeh'
    }
    this.hobbies = ['coding' , 'swimming' , 'playing bing ball'];
  }

  onClick(){
    this.name = 'hemo'
  }

  addHobby(hobby){
    this.hobbies.push(hobby);
    return false;
  }

  deleteHobby(hobby){
    for(let i=0 ; i<this.hobbies.length; i++){
      if(this.hobbies[i]==hobby){
        this.hobbies.splice(i,1);
      }

    }
  }
}

interface Address{
  street:string,
  city:string,
  state:string
}
